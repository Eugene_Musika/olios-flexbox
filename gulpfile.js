var gulp         = require('gulp'),
    postcss      = require('gulp-postcss'),
    sass         = require('gulp-sass'),
    autoprefixer = require('autoprefixer'),
    imagemin	 = require('gulp-imagemin'),
	pngquant 	 = require('imagemin-pngquant'),
    browser      = require('browser-sync'),
    sourcemaps   = require('gulp-sourcemaps');

gulp.task('sass', function () {
    return gulp.src('sass/*.scss')
       .pipe(sourcemaps.init())
       .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['node_modules/motion-ui/src']
            })
            .on('error', sass.logError))
        .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('_build/css'))
// for browser-sync
//        .pipe(browser.stream({match: '**/*.css'}));
});

//Image Task
//Compress
gulp.task('image', function() {
	return gulp.src('images/*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest('_build/images'));
});

// Builds the documentation and framework files
//gulp.task('build', ['clean', 'sass', 'javascript']);

// Starts a BrowerSync instance
/*
gulp.task('serve', ['sass'], function(){
    browser.init({
        server: {
            baseDir: "./"
        }
    });
});
*/

// Runs all of the above tasks and then waits for files to change
// for browser-sync you should add <, ['serve']> after <'default',>
gulp.task('default', function() {    
    gulp.watch(['sass/**/*.scss'], ['sass']);
	gulp.watch(['images/*.*'], ['image']);
// for browser-sync
//    gulp.watch('./**/*.html').on('change', browser.reload);
});